import React, { ReactElement } from 'react';

import { Title, Body } from './Dashboard.styles';
import { Props } from './Dashboard.config';

function Dashboard({ title, children }: Props): ReactElement {
  return (
    <div>
      <Title>{title}</Title>
      <Body>{children}</Body>
    </div>
  );
}

export default Dashboard;
