import { Meta } from '@storybook/react';

import Dashboard from './Dashboard';

export default {
  title: 'components/Dashboard',
  component: Dashboard,
} as Meta;
