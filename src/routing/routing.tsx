import React, { ReactElement } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Dashboard from '../pages/Dashboard';

function Routing(): ReactElement {
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <Switch>
        <Route exact path="/"><Dashboard title="Dashboard" /></Route>
      </Switch>
    </BrowserRouter>
  );
}

export default Routing;
