const promptDirectory = require('inquirer-directory');

module.exports = plop => {
  plop.setPrompt('directory', promptDirectory);

  plop.setGenerator('component', {
      description: 'Create a reusable component',
      prompts: [
        {
          type: 'directory',
          name: 'path',
          basePath: './src',
          message: 'Where you want to place your component?'
        },
        {
          type: 'input',
          name: 'name',
          message: 'What is your component name?'
        },
      ],
      actions: [
        {
          type: 'add',
          path: 'src/{{path}}/{{name}}/{{name}}.tsx',
          templateFile: 'plop-templates/component/component.tsx.hbs',
        },
        {
          type: 'add',
          path: 'src/{{path}}/{{name}}/{{name}}.styles.ts',
          templateFile: 'plop-templates/component/component.styles.ts.hbs',
        },
        {
          type: 'add',
          path: 'src/{{path}}/{{name}}/{{name}}.config.ts',
          templateFile: 'plop-templates/component/component.config.ts.hbs',
        },
        {
          type: 'add',
          path: 'src/{{path}}/{{name}}/{{name}}.test.ts',
          templateFile: 'plop-templates/component/component.test.ts.hbs',
        },
        {
          type: 'add',
          path: 'src/{{path}}/{{name}}/{{name}}.stories.tsx',
          templateFile: 'plop-templates/component/component.stories.tsx.hbs',
        },
        {
          type: 'add',
          path: 'src/{{path}}/{{name}}/index.ts',
          templateFile: 'plop-templates/component/index.ts.hbs',
        },
        // {
        //   // Action type 'append' injects a template into an existing file
        //   type: 'append',
        //   path: 'src/components/index.js',
        //   // Pattern tells plop where in the file to inject the template
        //   pattern: `/* PLOP_INJECT_IMPORT */`,
        //   template: `import {{pascalCase name}} from './{{pascalCase name}}';`,
        // },
        // {
        //   type: 'append',
        //   path: 'src/components/index.js',
        //   pattern: `/* PLOP_INJECT_EXPORT */`,
        //   template: `\t{{pascalCase name}},`,
        // },
      ],
    })
};
